# README #

A project created in my second year of university.

The goal is to solve an 8-Puzzle by implementing the A* algorithm, then to compare the use of different heuristics to see which one was the optimal choice.

The heuristics used here the following (and can be read about on the [Heuristics Wiki](https://heuristicswiki.wikispaces.com/)):

* Manhattan Distance
* Nilsson's Sequence Score
* Misplaced Tiles
* N-Swap
* Tiles out of Row and Column
* Edge Checking (a heuristic I created myself)

# Results #

The results showed that the best choice of heuristic was the Manhattan Distance as it was the fastest running heuristic (on average) that guaranteed the optimal path to the solution.

However, should the search space increase in size (for example on a puzzle that has a grid larger than 3x3), it may be better to use the Nilsson's Sequence Score if it's not completely necessary to find the optimal solution and the time taken to find the solution is a limiting factor. This is because, whilst it only found the optimal path 3 out of 10 times, it was always the fastest to find a path that solved the problem.