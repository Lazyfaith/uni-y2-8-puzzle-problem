import java.util.ArrayList;


public class Node {
	private static int numberOfNodesGenerated = 0;
	
	private static class Directions {
		public static int UP = 1;
		public static int DOWN = 2;
		public static int LEFT = 3;
		public static int RIGHT = 4;
	}
	
	public int id; // ID of the node, this is generated when the node is created
	public int parentNode; // ID of the parent node, this is will be generated
	public int depth; // How far down the tree the node is. The initial node will have a depth of 0 and it's child nodes will each have a depth of 1
	public ArrayList<Character> puzzleState;
	public int heuristicScore;
	
	public Node(int p, int d, ArrayList<Character> ps) {
		id = ++numberOfNodesGenerated;
		parentNode = p;
		depth = d;
		puzzleState = ps;
		heuristicScore = Heuristic.executeHeuristic(puzzleState);
	}
	
	public static int getNumberOfNodesGenerated() {
		return numberOfNodesGenerated;
	}
	public static void resetNumberOfNodesGenerated() {
		numberOfNodesGenerated = 0;
	}
	
	public int getTotalCost() {
		return depth + heuristicScore;
	}
	
	public ArrayList<Node> generatePossibleChildren() {
		// First, work out what moves can be done from the current state
		boolean canShiftTileUp = true;
		if(puzzleState.indexOf('0') > 5)
			canShiftTileUp = false;
		
		boolean canShiftTileDown = true;
		if(puzzleState.indexOf('0') < 3)
			canShiftTileDown = false;
		
		boolean canShiftTileLeft = true;
		if(puzzleState.indexOf('0') == 2 || puzzleState.indexOf('0') == 5 || puzzleState.indexOf('0') == 8)
			canShiftTileLeft = false;
		
		boolean canShiftTileRight = true;
		if(puzzleState.indexOf('0') == 0 || puzzleState.indexOf('0') == 3 || puzzleState.indexOf('0') == 6)
			canShiftTileRight = false;
		
		// Then perform those moves and return them as possible child nodes
		ArrayList<Node> children = new ArrayList<Node>();
		shiftTile(0);
		if(canShiftTileUp)
			children.add(new Node(this.id, this.depth+1, shiftTile(Directions.UP)));
		
		if(canShiftTileDown)
			children.add(new Node(this.id, this.depth+1, shiftTile(Directions.DOWN)));
		
		if(canShiftTileLeft)
			children.add(new Node(this.id, this.depth+1, shiftTile(Directions.LEFT)));
		
		if(canShiftTileRight)
			children.add(new Node(this.id, this.depth+1, shiftTile(Directions.RIGHT)));
		
		return children;
	}
	
	private ArrayList<Character> shiftTile(int direction) {
		// Create new state for us to do the change in
		ArrayList<Character> newState = new ArrayList<Character>();
		for(char c : puzzleState)
			newState.add(c);
		
		// Find 0
		int zeroLoc = newState.indexOf('0');
		
		// Perform shift
		if(direction == Directions.UP)
			swapTiles(newState, zeroLoc, zeroLoc+3);
		else if(direction == Directions.DOWN)
			swapTiles(newState, zeroLoc, zeroLoc-3);
		else if(direction == Directions.LEFT)
			swapTiles(newState, zeroLoc, zeroLoc+1);
		else if(direction == Directions.RIGHT)
			swapTiles(newState, zeroLoc, zeroLoc-1);
		
		return newState;
	}
	
	private void swapTiles(ArrayList<Character> state, int tile1, int tile2) {
		Character holder = state.get(tile1);
		state.set(tile1, state.get(tile2));
		state.set(tile2, holder);
	}
	
	public char[][] getStateIn2dArray() {
		char [][] map = new char[3][3];
		int c = 0;
		for(int x=0; x<3; x++) {
			for(int y=0; y<3; y++) {
				map[x][y] = puzzleState.get(c++); // puzzleState.charAt(c++);
			}
		}
		return map;
	}
}
