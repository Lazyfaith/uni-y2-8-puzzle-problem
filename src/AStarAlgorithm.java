import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class AStarAlgorithm {
	
	// The 10 puzzle starting states that were assigned to me
	// {puzzle number, puzzle start state}
	static String [][] assignedStartingStates = {
			{"10", "873065421"},
			{"19", "726840513"},
			{"27", "876043251"},
			{"29", "763084251"},
			{"14", "863451702"},
			{"22", "860473251"},
			{"13", "876520413"},
			{"25", "286703541"},
			{"6", "603841752"},
			{"20", "726403851"}
	};
	
	// The goal state
	public static ArrayList<Character> goalState = new ArrayList<Character>() {{
		add('1');
		add('2');
		add('3');
		add('8');
		add('0');
		add('4');
		add('7');
		add('6');
		add('5');
	}};
	
	ArrayList<Character> startState;
	
	ArrayList<Node> openNodes = new ArrayList<Node>();
	ArrayList<Node> closedNodes = new ArrayList<Node>();
	
	long timeToComplete;
	
	public AStarAlgorithm(int puzzleStartStateFlag, int heuristicFlag) throws Exception {
		// Ensure the given flags are within their respective ranges
		if(puzzleStartStateFlag < 0 || puzzleStartStateFlag >= assignedStartingStates.length
			|| heuristicFlag < 0 || heuristicFlag > Heuristic.availableHeuristics.length)
			throw new Exception("Invalid puzzle or heuristic selection.");
		
		// Convert chosen start state to an ArrayList<Character> obj
		String startStateString = assignedStartingStates[puzzleStartStateFlag][1];
		startState = new ArrayList<Character>();
		for(int i=0; i<startStateString.length(); i++)
			startState.add(startStateString.charAt(i));
		
		// Get the heuristic the user chose
		Heuristic.setHeuristicChoice(heuristicFlag);
	}
	
	
		
	
	
	
	
	public Node start() {
		long startTime = System.currentTimeMillis();
		// Create the initial node and add it to the open list
		openNodes.add(new Node(-1, 0, startState));
		
		int t = 0;
		while(!openNodes.isEmpty()) {
			// Find the best open node
			Collections.sort(openNodes, new NodeComparator());
			Node currentBestNode = openNodes.remove(0);
			closedNodes.add(currentBestNode);
			
			// Check if the node is the goal
			if (currentBestNode.puzzleState.equals(goalState)) {
				timeToComplete = System.currentTimeMillis() - startTime;
				return currentBestNode;
			}
			
			// Create & process all possible children of current bestNode
			ArrayList<Node> children = currentBestNode.generatePossibleChildren();
			
			for(Node newNode : children) {
				// Check if this state has been covered by another node already
				Node nodeWithSameState = null;
				for(Node existingNode : openNodes) {
					if (existingNode.puzzleState.equals(newNode.puzzleState))
						nodeWithSameState = existingNode;
				}
				for(Node existingNode: closedNodes) {
					if (existingNode.puzzleState.equals(newNode.puzzleState))
						nodeWithSameState = existingNode;
				}
				
				if (nodeWithSameState == null) {
					// No other nodes have the same state as this node, it is brand new
					// Add this node to the list of open nodes
					openNodes.add(newNode);
				}
				else {
					// Another node shares the same state as this new one
					// If this new node is more efficient than the other (i.e. has less depth) then replace the existing node with newNode
					if (newNode.depth < nodeWithSameState.depth) {
						// Is indeed more efficient, remove old node and place newNode in openNodes
						closedNodes.remove(nodeWithSameState);
						openNodes.remove(nodeWithSameState);
						
						openNodes.add(newNode);		
					}
				}
			}
		}
		
		// No solution was found
		return null;
	}
	
			
	
	
	
	

	public static void main(String[] args) {
		// Ask user which puzzle they'd like to solve
		System.out.println("Available puzzle start states:\n");
		
		for(int i=0; i<assignedStartingStates.length; i++) {
			System.out.println(i + " - Puzzle " + assignedStartingStates[i][0] + "\t" + assignedStartingStates[i][1]);
		}
		System.out.println(assignedStartingStates.length+1 + " - All puzzles");
		
		Scanner input = new Scanner(System.in);
		int chosenPuzzle = input.nextInt();
		
		// Ask user which heuristic to use
		System.out.println("Available heuristics:\n");
		
		for(int i=0; i<Heuristic.availableHeuristics.length; i++) {
			System.out.println(i + " - " + Heuristic.availableHeuristics[i]);
		}
		
		int chosenHeuristic = input.nextInt();
		input.close();
			
		if (chosenPuzzle != 11) {
			// Solve the puzzle with the chosen heuristic
			AStarAlgorithm algorithm;
			try {
				algorithm = new AStarAlgorithm(chosenPuzzle, chosenHeuristic);
			}
			catch (Exception ex) {
				System.out.println(ex.getMessage());
				return;
			}
			
			System.out.println("Starting hunt...");
			Node endNode = algorithm.start();
			System.out.println();
			
			if (endNode != null) {
				System.out.println("Solution found");
				System.out.println("Puzzle " + AStarAlgorithm.assignedStartingStates[chosenPuzzle][0]);
				System.out.println("Solved using:\t" + Heuristic.availableHeuristics[chosenHeuristic]);
				System.out.println("Steps = " + endNode.depth + "\tNodes considered = " + Node.getNumberOfNodesGenerated() + "\tMilliseconds = " + algorithm.timeToComplete);
			}
			else {
				System.out.println("Could not find solution");
				System.out.println("Puzzle " + AStarAlgorithm.assignedStartingStates[chosenPuzzle][0]);
				System.out.println("Nodes considered = " + Node.getNumberOfNodesGenerated());
			}
		}
		else {
			// Solve all puzzles with the chosen heuristic
			System.out.println("Solving using:\t" + Heuristic.availableHeuristics[chosenHeuristic]);
			for(int puzzle=0; puzzle<AStarAlgorithm.assignedStartingStates.length; puzzle++) {
				AStarAlgorithm algorithm;
				try {
					algorithm = new AStarAlgorithm(puzzle, chosenHeuristic);
				}
				catch (Exception ex) {
					System.out.println(ex.getMessage());
					return;
				}
				
				Node endNode = algorithm.start();
				if (endNode != null)
					System.out.println("Puzzle " + AStarAlgorithm.assignedStartingStates[puzzle][0] + "\t" + endNode.depth + "\t" + Node.getNumberOfNodesGenerated() + "\t" + algorithm.timeToComplete);
				else
					System.out.println("Puzzle " + AStarAlgorithm.assignedStartingStates[puzzle][0] + "\tNO SOLUTION FOUND");
				
				Node.resetNumberOfNodesGenerated();
			}
			System.out.println("END");
		}
	}

}
