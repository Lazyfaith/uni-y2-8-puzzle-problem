import java.util.ArrayList;


public class Heuristic {
	public static String [] availableHeuristics = {
			"Manhattan Distance",
			"Nilsson's Sequence Score",
			"Misplaced Tiles",
			"N-Swap",
			"Tiles of out Row and Column",
			"Edge Checking",
			"No heuristic (breadth first search)"
	};
	
	static int heuristicChoice;
	
	public static void setHeuristicChoice(int flag) {
		heuristicChoice = flag;
	}
	
	public static int executeHeuristic(ArrayList<Character> puzzleState) {
		switch (heuristicChoice) {
			case 0: return heuristicManhattanDistance(puzzleState);
			case 1: return heuristicNilssonsSequenceScore(puzzleState);
			case 2: return heuristicMisplacedTiles(puzzleState);
			case 3: return heuristicNSwap(puzzleState);
			case 4: return heuristicTilesOutOfRowAndColumn(puzzleState);
			case 5: return heuristicEdgeChecking(puzzleState);
			case 6: return 0; // No heuristic, so just return 0
			default: return -1; // This case will never happen
		}
	}
	
	private static int heuristicManhattanDistance(ArrayList<Character> puzzleState) {
		int manhattanDistance = 0;
		for(Character c : puzzleState) {
			int xDiff = Math.abs((puzzleState.indexOf(c) % 3) - (AStarAlgorithm.goalState.indexOf(c) % 3));
			int yDiff = Math.abs((puzzleState.indexOf(c) / 3) - (AStarAlgorithm.goalState.indexOf(c) / 3));
			manhattanDistance += xDiff + yDiff;
		}
		return manhattanDistance;
	}
	
	private static int heuristicNilssonsSequenceScore(ArrayList<Character> puzzleState) {
		// Get the manhattan distance
		int manhattanDistance = heuristicManhattanDistance(puzzleState);
		
		// Array indexes for moving around the 1D arraylist like we're moving around the outside of the 3x3 grid
		int [] order = {0, 1, 2, 5, 8, 7, 6, 3, 4};
		
		// Now move around the grid to work out the sequence score
		int sequenceScore = 0;
		for(int i=0; i<order.length-1; i++) {
			if(Character.getNumericValue(puzzleState.get(order[i])) != Character.getNumericValue(puzzleState.get(order[i+1]))-1)
				sequenceScore += 2;
		}
		if (puzzleState.get(4) != '0')
			sequenceScore++;
		
		return manhattanDistance + (3 * sequenceScore);
	}
	
	private static int heuristicMisplacedTiles(ArrayList<Character> puzzleState) {
		int h = 0;
		for(int i=0; i<puzzleState.size(); i++) {
			if (!puzzleState.get(i).equals(AStarAlgorithm.goalState.get(i)))
				h++;
		}
		return h;
	}
	
	private static int heuristicNSwap(ArrayList<Character> puzzleState) {
		// Make a copy of the puzzleState for us to edit
		ArrayList<Character> swappablePuzzle = new ArrayList<Character>();
		for(Character c : puzzleState)
			swappablePuzzle.add(c);
		// Go through all tiles
		int swaps = 0;
		for(int i=0; i<swappablePuzzle.size(); i++) {
			// Check if tile is in wrong place
			int goalLoc = AStarAlgorithm.goalState.indexOf(swappablePuzzle.get(i));
			if (i != goalLoc) {
				// Swap current tile with whichever tile is in this tile's goal location
				Character holder = swappablePuzzle.get(goalLoc);
				swappablePuzzle.set(goalLoc, swappablePuzzle.get(i));
				swappablePuzzle.set(i, holder);
				i--; // There's a new tile in the current location, so let's check if we need to move this one too
				swaps++;
			}
		}
		return swaps;
	}
	
	private static int heuristicTilesOutOfRowAndColumn(ArrayList<Character> puzzleState) {
		int h = 0;
		for(Character c : puzzleState) {
			// Check if tile is in the correct column
			if ((puzzleState.indexOf(c) % 3) != (AStarAlgorithm.goalState.indexOf(c) % 3))
					h++;
			// Check if tile is in the correct row
			if ((puzzleState.indexOf(c) / 3) != (AStarAlgorithm.goalState.indexOf(c) / 3))
					h++;
		}
		return h;
	}
	
	private static int heuristicEdgeChecking(ArrayList<Character> puzzleState) {
		int [] cost = {5, 2, 5, 2, 0, 2, 5, 2, 5};
		int h = 0;
		for(int i=0; i<puzzleState.size(); i++) {
			if (!puzzleState.get(i).equals(AStarAlgorithm.goalState.get(i)))
				h += cost[i];
		}
		return h;
	}
}
